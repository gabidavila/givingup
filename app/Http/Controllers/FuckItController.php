<?php

namespace App\Http\Controllers;

class FuckItController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $indifference = array(
            'Why bother?',
            'Who cares?',
            'I give up.',
            'No one would care if this service died.',
            'Why does it even matter?',
            'I don\'t care any more.'
        );

        $apathy = array_rand($indifference);
        return response()->json(['apathy' => $indifference[$apathy]]);
    }
}
